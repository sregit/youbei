package db

import (
	"os"

	Zipz "youbei/utils/zip"
)

//SQLDumpZip ...
func SQLDumpZip(backupfilePath, zipPath, zipPassword string) (string, error) {
	err := Zipz.Zip(backupfilePath, zipPath, zipPassword)
	if err != nil {
		return zipPath, err
	}
	err = os.RemoveAll(backupfilePath)
	return zipPath, err
}
