package db

import (
	"time"
	//mysql ...
	"gitee.com/countpoison_admin/xorm"
	_ "github.com/go-sql-driver/mysql"
)

// MysqlConnectTest ...
func MysqlConnectTest(Host, Port, Dbname, User, Password, Char string) error {
	db, err := xorm.NewEngine("mysql", User+":"+Password+"@("+Host+":"+Port+")/"+Dbname+"?charset="+Char)
	defer db.Close()
	if err != nil {
		return err
	}
	err = db.Ping()
	return err
}

// MysqlSQLDump ...
func MysqlSQLDump(host, port, dbname, user, password, char, backupfilePath, zippwd string) (string, error) {
	db, err := xorm.NewEngine("mysql", user+":"+password+"@("+host+":"+port+")/"+dbname+"?charset="+char)
	defer db.Close()
	if err != nil {
		return "", err
	}
	err = db.Ping()
	if err != nil {
		return "", err
	}
	nowtime := time.Now().Format("2006-01-02_15-04-05")
	dist := host + "_mysql_" + dbname + "_" + char + "_" + nowtime + ".sql"
	distzp := host + "_mysql_" + dbname + "_" + char + "_" + nowtime + ".zip"
	err = db.DumpAllToFile(backupfilePath + "/" + dist)
	if err != nil {
		return "", err
	}
	return SQLDumpZip(backupfilePath+"/"+dist, backupfilePath+"/"+distzp, zippwd)
}
