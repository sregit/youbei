package db

import (
	"strings"
	"time"

	//go-sqlite3
	"gitee.com/countpoison_admin/xorm"
	_ "github.com/mattn/go-sqlite3"
)

//SqliteConnectTest ...
func SqliteConnectTest(sqlpath string) error {
	db, err := xorm.NewEngine("sqlite3", sqlpath)
	defer db.Close()
	if err != nil {
		return err
	}
	err = db.Ping()
	return err
}

//SqliteSQLDump ...
func SqliteSQLDump(dbpath, backupfilePath, zippwd string) (string, error) {
	db, err := xorm.NewEngine("sqlite3", dbpath)
	defer db.Close()
	if err != nil {
		return "", err
	}
	err = db.Ping()
	if err != nil {
		return "", err
	}
	nowtime := time.Now().Format("2006-01-02_15-04-05")
	dbstr := strings.Split(dbpath, "/")
	dist := "localhost_sqlite_" + dbstr[len(dbstr)-1] + "_" + nowtime + ".sql"
	distzp := "localhost_sqlite_" + dbstr[len(dbstr)-1] + "_" + nowtime + ".zip"
	err = db.DumpAllToFile(backupfilePath + "/" + dist)
	if err != nil {
		return "", err
	}
	return SQLDumpZip(backupfilePath+"/"+dist, backupfilePath+"/"+distzp, zippwd)
}
