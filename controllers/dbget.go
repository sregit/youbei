package controllers

import (
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gitee.com/countpoison_admin/xorm"
)

type dbinfo struct {
	User     string
	Password string
	Char     string
}

func DBGet(c *gin.Context) {
	User := c.Query("User")
	Password := c.Query("Password")
	Host := c.Query("Host")
	Port := c.Query("Port")
	Char := c.Query("Char")

	db, err := xorm.NewEngine("mysql", User+":"+Password+"@("+Host+":"+Port+")/?charset="+Char)
	if err != nil {
		APIReturn(c, 500, "连接数据库失败", err)
		return
	}
	dbs := []string{}
	if err := db.SQL("show databases;").Find(&dbs); err != nil {
		APIReturn(c, 500, "查询数据库失败", err)
		return
	}
	db.Close()
	APIReturn(c, 200, "成功", dbs)
}
