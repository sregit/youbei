package controllers

import (
	md "youbei/models"

	jobs "youbei/utils/jobs"

	"github.com/gin-gonic/gin"
)

func JobTest(c *gin.Context) {
	ob := new(md.Task)
	if err := c.Bind(ob); err != nil {
		APIReturn(c, 500, "解析失败", err.Error())
		return
	}
	zipfile, err := jobs.TestBackup(ob)
	if err != nil {
		APIReturn(c, 500, "备份失败", err.Error())
		return
	}
	rs := []md.RemoteStorage{}
	if len(ob.RS) > 0 {
		if err := md.Localdb().In("id", ob.RS).Find(&rs); err != nil {
			APIReturn(c, 500, "查询远程存储失败", err.Error())
			return
		}
	}
	for _, v := range rs {
		if err := jobs.TestRemote(zipfile, v); err != nil {
			APIReturn(c, 500, v.Name+"存储失败", err.Error())
			return
		}
	}
	APIReturn(c, 200, "测试成功", nil)
}

func RunJob(c *gin.Context) {
	if err := jobs.Backup(c.Param("id"), true); err != nil {
		APIReturn(c, 500, "失败", err.Error())
		return
	}
	APIReturn(c, 200, "执行成功", nil)
}
